import React, { useRef, useEffect, useState, useCallback } from "react";

import { Link, useLocation, useNavigate } from "react-router-dom";

import "./header.scss";

import logo from "../../assets/n.png";

import { category } from "../../api/tmdbApi";

import Button from "../button/Button";

import Input from "../input/Input";

// ICONS BOOTSTRAP
import * as Icon from "react-bootstrap-icons";

// MUI MATERIAL
import {
  Box,
  Grid,
  IconButton,
  InputAdornment,
  Modal,
  TextField,
  Typography,
} from "@mui/material";

// ICONS MUI
import {
  LocalPostOfficeRounded,
  PersonRounded,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";

// NAVBAR ITEMS
const headerNav = [
  {
    display: "Home",
    path: "/",
  },
  {
    display: "Movies",
    path: "/movie",
  },
  {
    display: "TV Series",
    path: "/tv",
  },
];

// SEARCH FUNCTION IN NAVBAR
const MovieSearch = (props) => {
  const navigate = useNavigate();

  const [keyword, setKeyword] = useState(props.keyword ? props.keyword : "");

  const goToSearch = useCallback(() => {
    if (keyword.trim().length > 0) {
      navigate(`/${category.movie}/search/${keyword}`);
    }
  }, [keyword, navigate]);

  useEffect(() => {
    const enterEvent = (e) => {
      e.preventDefault();
      if (e.keyCode === 13) {
        goToSearch();
      }
    };
    document.addEventListener("keyup", enterEvent);
    return () => {
      document.removeEventListener("keyup", enterEvent);
    };
  }, [keyword, goToSearch]);

  return (
    <div className="movie-search">
      <Input
        type="text"
        placeholder="Enter keyword"
        value={keyword}
        onChange={(e) => setKeyword(e.target.value)}
      />
      <Button className="small" onClick={goToSearch}>
        <Icon.Search color="white" />
      </Button>
    </div>
  );
};

// HEADER
const Header = ({ user }) => {
  const { pathname } = useLocation();
  const location = useLocation();
  const headerRef = useRef(null);

  const active = headerNav.findIndex((e) => e.path === pathname);

  // LOGIN PASSPORT JS
  const google = () => {
    window.open("http://localhost:5000/auth/google", "_self");
  };

  // LOGOUT PASSPORT JS
  const logout = () => {
    window.open("http://localhost:5000/auth/logout", "_self");
  };

  // MODAL BOX FOR LOGIN
  const [openModalLogin, setOpenModalLogin] = useState(false);
  const handleOpenLogin = () => {
    setOpenModalLogin(true);
  };
  const handleCloseLogin = () => {
    setOpenModalLogin(false);
  };

  // MODAL BOX FOR REGISTER
  const [openModalRegister, setOpenModalRegister] = useState(false);
  const handleOpenRegister = () => {
    setOpenModalRegister(true);
  };
  const handleCloseRegister = () => {
    setOpenModalRegister(false);
  };

  // STYLE FOR MODAL BOX
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "400px",
    bgcolor: "#fff",
    borderRadius: "1rem",
    boxShadow: 24,
    p: 4,
    color: "#000",
  };

  // BUTTON TOGGLE FOR SHOW PASSWORD
  const [showPassword, setShowPassword] = React.useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  useEffect(() => {
    const shrinkHeader = () => {
      if (
        document.body.scrollTop > 100 ||
        document.documentElement.scrollTop > 100
      ) {
        headerRef.current.classList.add("shrink");
      } else {
        headerRef.current.classList.remove("shrink");
      }
    };
    window.addEventListener("scroll", shrinkHeader);
    return () => {
      window.removeEventListener("scroll", shrinkHeader);
    };
  }, []);

  return (
    <div ref={headerRef} className="header">
      <div className="header__wrap container">
        <div className="logo">
          <Link to="/">
            <img src={logo} alt="" />
          </Link>
        </div>
        <MovieSearch />

        {user ? (
          <ul className="list">
            <li className="listItem">{user.displayName}</li>
            <Button className="btns" onClick={logout}>
              Logout
            </Button>
          </ul>
        ) : (
          <div>
            <Button className="btns" onClick={handleOpenLogin}>
              login
            </Button>
            <Button className="btns" onClick={handleOpenRegister}>
              Register
            </Button>
          </div>

          //    <button className="btns" onClick={handleOpenRegister}>
          //    Register
          //  </button>
        )}

        <Modal
          open={openModalLogin}
          onClose={handleCloseLogin}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Typography
              id="modal-modal-title"
              variant="h6"
              component="h2"
              sx={{ textAlign: "center" }}
            >
              Login
            </Typography>
            <Grid container direction="column" spacing={2} sx={{ mt: 3 }}>
              <Grid item>
                <TextField
                  label="Username"
                  variant="outlined"
                  fullWidth
                  focused
                  type="text"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <LocalPostOfficeRounded />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Password"
                  variant="outlined"
                  fullWidth
                  type={showPassword ? "text" : "password"}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item>
                <Button className="btns" onClick={google}>
                  Login Google
                </Button>
              </Grid>
              <Grid item sx={{ mt: 2 }}>
                <Button className="btns">I'm in</Button>
              </Grid>
            </Grid>
          </Box>
        </Modal>

        <Modal
          open={openModalRegister}
          onClose={handleCloseRegister}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Typography
              id="modal-modal-title"
              variant="h6"
              component="h2"
              sx={{ textAlign: "center" }}
            >
              Register
            </Typography>
            <Grid container direction="column" spacing={2} sx={{ mt: 3 }}>
              <Grid item>
                <TextField
                  label="First Name"
                  variant="outlined"
                  fullWidth
                  focused
                  type="text"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <PersonRounded />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Last Name"
                  variant="outlined"
                  fullWidth
                  type="text"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <PersonRounded />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Username"
                  variant="outlined"
                  fullWidth
                  type="text"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <LocalPostOfficeRounded />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Password"
                  variant="outlined"
                  fullWidth
                  type={showPassword ? "text" : "password"}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item>
                <TextField
                  label="Password Confirmation"
                  variant="outlined"
                  fullWidth
                  type={showPassword ? "text" : "password"}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                        >
                          {showPassword ? <VisibilityOff /> : <Visibility />}
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item>
                <Button className="btns">Login Biasa</Button>
              </Grid>
            </Grid>
          </Box>
        </Modal>
      </div>
    </div>
  );
};

export default Header;
