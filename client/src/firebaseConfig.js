// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyB0tLmzylhjGVZUrGoRTqN4dconGuBCDvM",
  authDomain: "auth-project-b9f2b.firebaseapp.com",
  projectId: "auth-project-b9f2b",
  storageBucket: "auth-project-b9f2b.appspot.com",
  messagingSenderId: "1036407916284",
  appId: "1:1036407916284:web:a3cdc7960d10c9e319b979",
  measurementId: "G-P9X6TER3ZY",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
